package asgn2Customers;

import asgn2Exceptions.CustomerException;

/** An abstract class to represent a customer at the Pizza Palace restaurant.
 *  The Customer class is used as a base class of PickUpCustomer, 
 *  DriverDeliveryCustomer and DroneDeliverCustomer. Each of these subclasses overwrites
 *  the abstract method getDeliveryDistance. A description of the class's
 * fields and their constraints is provided in Section 5.2 of the Assignment Specification.  
 * 
 * @author n9702911
*/
public abstract class Customer {
	private static final int MAXIMUM_BLOCK = 10;
	private static final int MAXIMUM_NAME = 20;
	private static final int MINIMUM_NAME = 1;
	private static final int MOBILE_LENGTH = 10;
	private String customerName;
	private String customerMobileNum;
	private int custLocationX;
	private int custLocationY;
	private String customerType;


	/**
	 *  This class represents a customer of the Pizza Palace restaurant.  A detailed description of the class's fields
	 *  and parameters is provided in the Assignment Specification, in particular in Section 5.2. 
	 *  A CustomerException is thrown if the any of the constraints listed in Section 5.2 of the Assignment Specification
	 *  are violated. 
	 *  
  	 * <P> PRE: True
  	 * <P> POST: All field values are set
  	 * 
	 * @param name - The Customer's name 
	 * @param mobileNumber - The customer mobile number
	 * @param locationX - The customer x location relative to the Pizza Palace Restaurant measured in units of 'blocks' 
	 * @param locationY - The customer y location relative to the Pizza Palace Restaurant measured in units of 'blocks' 
	 * @param type - A human understandable description of this Customer type
	 * @throws CustomerException if mobile number is not 10 characters long
	 * @throws CustomerException if the name is not between 1 and 20 characters long
	 * @throws CustomerException if the customer location is more than 10 blocks North, East, South or West
	 * @throws CustomerException if the mobile number does not start with a 0
	 * @throws CustomerException if the mobile number is not 10 digits
	 * 
	 */
	public Customer(String name, String mobileNumber, int locationX, int locationY, String type) throws CustomerException{
		//Throw an exception if mobile number is not the desired length
		if(mobileNumber.length() != MOBILE_LENGTH)
			throw new CustomerException("Mobile number must be 10 digits long");
		//Throw an exception if name is not between the desired length
		else if(name.length() < MINIMUM_NAME || name.length() > MAXIMUM_NAME)
			throw new CustomerException("Customer name must be between 1 and 20 characters long");
		//Throw an exception if the customer is not 10 blocks North, East, South or West from the Restaurant
		else if(locationX > MAXIMUM_BLOCK || locationY > MAXIMUM_BLOCK || locationX < -MAXIMUM_BLOCK || locationY < -MAXIMUM_BLOCK)
			throw new CustomerException("Customer cannot be more than 10 blocks north, east, south or west "
					+ "from the restaurant for delivery");
		//Throw an exception if the mobile number does not start with 0
		else if(!mobileNumber.startsWith("0"))
			throw new CustomerException("Mobile number must start with 0");
		//Throw an exception if the mobile number is not all digits
		else if(!mobileNumber.matches("^[0-9]{10}$"))
			throw new CustomerException("Mobile number must be be 10 digits");
		
		customerName = name;
		customerMobileNum = mobileNumber;
		custLocationX = locationX;
		custLocationY = locationY;
		customerType = type;
	}
	
	/**
	 * Returns the Customer's name.
	 * @return The Customer's name.
	 */
	public final String getName(){
		return customerName;
	}
	
	/**
	 * Returns the Customer's mobile number.
	 * @return The Customer's mobile number.
	 */
	public final String getMobileNumber(){
		return customerMobileNum;
	}

	/**
	 * Returns a human understandable description of the Customer's type. 
	 * The valid alternatives are listed in Section 5.2 of the Assignment Specification. 
	 * @return A human understandable description of the Customer's type.
	 */
	public final String getCustomerType(){
		return customerType;
	}
	
	/**
	 * Returns the Customer's X location which is the number of blocks East or West 
	 * that the Customer is located relative to the Pizza Palace restaurant. 
	 * @return The Customer's X location
	 */
	public final int getLocationX(){
		return custLocationX;
	}

	/**
	 * Returns the Customer's Y location which is the number of blocks North or South 
	 * that the Customer is located relative to the Pizza Palace restaurant. 
	 * @return The Customer's Y location
	 */
	public final int getLocationY(){
		return custLocationY;
	}

	/**
	 * An abstract method that returns the distance between the Customer and 
	 * the restaurant depending on the mode of delivery. 
	 * @return The distance between the restaurant and the Customer depending on the mode of delivery.
	 */
	public abstract double getDeliveryDistance();

	
	
	/**
	 * Compares *this* Customer object with an instance of an *other* Customer object and returns true if  
	 * if the two objects are equivalent, that is, if the values exposed by public methods are equal.
	 *  You do not need to test this method.
	 * 
	 * @return true if *this* Customer object and the *other* Customer object have the same values returned for 	
	 * getName(),getMobileNumber(),getLocationX(),getLocationY(),getCustomerType().
	 */
	@Override
	public boolean equals(Object other){
		Customer otherCustomer = (Customer) other;

		return ( (this.getName().equals(otherCustomer.getName()))  &&
			(this.getMobileNumber().equals(otherCustomer.getMobileNumber())) && 
			(this.getLocationX() == otherCustomer.getLocationX()) && 
			(this.getLocationY() == otherCustomer.getLocationY()) && 
			(this.getCustomerType().equals(otherCustomer.getCustomerType())) );			
	}
}
