package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.PizzaTopping;
import asgn2Restaurant.PizzaRestaurant;

/**
 * A class that tests the methods relating to the handling of Pizza objects in the asgn2Restaurant.PizzaRestaurant class as well as
 * processLog and resetDetails.
 * 
 * @author n9702911
 *
 */
public class RestaurantPizzaTests {
	
	//Test that an empty restaurant can be created
	@Test@Before
	public void createRestaurant() {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		assertEquals(0, testRestaurant.getNumPizzaOrders());
	}
	
	//Test that a pizza dataset is created
	@Test
	public void createPizzaDataset() throws CustomerException, PizzaException, LogHandlerException {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("logs/20170101.txt");
		assertEquals(3, testRestaurant.getNumPizzaOrders());
	}
	
	//Test that a pizza dataset is created
	@Test
	public void createPizzaDataset2() throws CustomerException, PizzaException, LogHandlerException {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("logs/20170102.txt");
		assertEquals(10, testRestaurant.getNumPizzaOrders());
	}
	
	//Test that a pizza dataset is created
	@Test
	public void createPizzaDataset3() throws CustomerException, PizzaException, LogHandlerException {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("logs/20170103.txt");
		assertEquals(100, testRestaurant.getNumPizzaOrders());
	}
	
	//Test the the total profit is calculated correctly
	@Test
	public void calculatePizzaDataset1() throws CustomerException, PizzaException, LogHandlerException {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("logs/20170101.txt");
		assertEquals(36.5, testRestaurant.getTotalProfit(), 0.1);
	}
	
	//Test that pizza dataset is reset to empty
	@Test
	public void resetPizzas() throws CustomerException, PizzaException, LogHandlerException {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("logs/20170101.txt");
		testRestaurant.resetDetails();
		assertEquals(0, testRestaurant.getNumPizzaOrders());
	}
	
	//Test that a pizza object is returned
	@Test 
	public void getPizzaByIndex() throws CustomerException, PizzaException, LogHandlerException {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("logs/testGetPizza.txt");
		assertTrue(testRestaurant.getPizzaByIndex(0).equals(testRestaurant.getPizzaByIndex(3)));
	}
	
	//Test that an exception is thrown when trying to get a pizza with an invalid index
	@Test(expected = PizzaException.class)
	public void getPizzaByIndexException() throws CustomerException, PizzaException, LogHandlerException {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("logs/20170101.txt");
		testRestaurant.getPizzaByIndex(3);
	}
	
	//Test that an exception is rethrown
	@Test(expected = PizzaException.class)
	public void invalidPizzaType() throws CustomerException, PizzaException, LogHandlerException {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("logs/test.txt");
	}
	
	//Test that an exception is rethrown
	@Test(expected = LogHandlerException.class)
	public void fileNotFoundException() throws CustomerException, PizzaException, LogHandlerException {
		PizzaRestaurant testRestaurant = new PizzaRestaurant();
		testRestaurant.processLog("logs/someLog.txt");
	}
}
