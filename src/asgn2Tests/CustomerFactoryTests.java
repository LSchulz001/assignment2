package asgn2Tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;

/**
 * A class the that tests the asgn2Customers.CustomerFactory class.
 * 
 * @author n9699155
 *
 */
public class CustomerFactoryTests {
	//Check that a pickup customer can be created via the factory
	@Test
	public void FactoryPickupCustomer() throws CustomerException {
		Customer cusTest = CustomerFactory.getCustomer("PUC", "TestName", "0412341234", 0, 0);
		assertEquals("Pick Up", cusTest.getCustomerType());
	}
	
	//Check that a Drone Delivery customer can be created via the factory	
	@Test
	public void FactoryDroneCustomer() throws CustomerException {
		Customer cusTest = CustomerFactory.getCustomer("DNC", "TestName", "0412341234", 5, -5);
		assertEquals("Drone Delivery", cusTest.getCustomerType());
	}
	//Check that a Driver Delivery customer can be created via the factory	
	@Test
	public void FactoryDriverCustomer() throws CustomerException {
		Customer cusTest = CustomerFactory.getCustomer("DVC", "TestName", "0412341234", 5, -5);
		assertEquals("Driver Delivery", cusTest.getCustomerType());
	}
	
	//Check that an exception is thrown if the factory is given a non-existent customerCode
	@Test(expected = CustomerException.class)
	public void CustomerFactoryBadCode() throws CustomerException {
		Customer cusTest = CustomerFactory.getCustomer("DUC", "TestName", "0412341234", 5, -5);
	}
}
