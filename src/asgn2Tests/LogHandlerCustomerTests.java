package asgn2Tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Restaurant.LogHandler;

/**
 * A class that tests the methods relating to the creation of Customer objects in the asgn2Restaurant.LogHander class.
 *
 * @author n9699155
 */
public class LogHandlerCustomerTests {
	//Test that we can successfully create a dataset with the correct size from each of the given logs
	@Test
	public void createCustomerDataset() throws CustomerException, LogHandlerException {
		ArrayList<Customer> customerLogTest = new ArrayList<Customer>();
		customerLogTest = LogHandler.populateCustomerDataset("logs/20170101.txt");
		assertEquals(3, customerLogTest.size());
	}
	
	@Test
	public void createCustomerDataset2() throws CustomerException, LogHandlerException {
		ArrayList<Customer> customerLogTest = new ArrayList<Customer>();
		customerLogTest = LogHandler.populateCustomerDataset("logs/20170102.txt");
		assertEquals(10, customerLogTest.size());
	}
	
	@Test
	public void createCustomerDataset3() throws CustomerException, LogHandlerException {
		ArrayList<Customer> customerLogTest = new ArrayList<Customer>();
		customerLogTest = LogHandler.populateCustomerDataset("logs/20170103.txt");
		assertEquals(100, customerLogTest.size());
	}
	
	//Test that an exception is thrown when LogHandler is given a non-existent log
	@Test(expected = LogHandlerException.class)
	public void customerFileNotFoundException() throws CustomerException, LogHandlerException {
		ArrayList<Customer> customerLogTest = new ArrayList<Customer>();
		customerLogTest = LogHandler.populateCustomerDataset("logs/somelog.txt");
	}
	
	//Test that an exception is thrown when an IOException occurs
	@Test(expected = LogHandlerException.class)
	public void customerIOException() throws CustomerException, LogHandlerException, CustomerException, IOException {
		ArrayList<Customer> customerLogTest = new ArrayList<Customer>();
		File testFile = new File("logs/20170101.txt");
		final RandomAccessFile raFile = new RandomAccessFile(testFile, "rw");
		//Lock stream (file still in use)
		raFile.getChannel().lock();
		customerLogTest = LogHandler.populateCustomerDataset("logs/20170101.txt");
	}
	
	//Test that an exception is rethrown	
	@Test(expected = CustomerException.class) 
	public void invalidCustomerType() throws CustomerException, LogHandlerException, IOException {
		ArrayList<Customer> customerLogTest = new ArrayList<Customer>();
		customerLogTest = LogHandler.populateCustomerDataset("logs/mytest.txt");
	}
	
	//Test that an exception is rethrown
	@Test(expected = CustomerException.class) 
	public void invalidCustomerLocation() throws CustomerException, LogHandlerException, IOException {
		ArrayList<Customer> customerLogTest = new ArrayList<Customer>();
		customerLogTest = LogHandler.populateCustomerDataset("logs/mytest2.txt");
	}
	
	//Test that an exception is rethrown
	@Test(expected = CustomerException.class) 
	public void invalidCustomerName() throws CustomerException, LogHandlerException, IOException {
		ArrayList<Customer> customerLogTest = new ArrayList<Customer>();
		customerLogTest = LogHandler.populateCustomerDataset("logs/mytest3.txt");
	}
	
	//Test that an exception is rethrown
	@Test(expected = CustomerException.class) 
	public void invalidCustomerPhone() throws CustomerException, LogHandlerException, IOException {
		ArrayList<Customer> customerLogTest = new ArrayList<Customer>();
		customerLogTest = LogHandler.populateCustomerDataset("logs/mytest4.txt");
	}
	
	//Test that an exception is rethrown
	@Test(expected = CustomerException.class) 
	public void CustomerPhoneTooLong() throws CustomerException, LogHandlerException, IOException {
		ArrayList<Customer> customerLogTest = new ArrayList<Customer>();
		customerLogTest = LogHandler.populateCustomerDataset("logs/mytest5.txt");
	}
	
	//Test that an exception is rethrown
	@Test(expected = CustomerException.class) 
	public void CustomerPhoneTooShort() throws CustomerException, LogHandlerException, IOException {
		ArrayList<Customer> customerLogTest = new ArrayList<Customer>();
		customerLogTest = LogHandler.populateCustomerDataset("logs/mytest6.txt");
	}
}
