package asgn2Tests;

import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

import org.junit.Test;

import asgn2Customers.Customer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Restaurant.LogHandler;

/** A class that tests the methods relating to the creation of Pizza objects in the asgn2Restaurant.LogHander class.
* 
* @author n9702911
* 
*/
public class LogHandlerPizzaTests {
	
	//Test that example dataset 1 can be created
	@Test
	public void createPizzaDataset() throws PizzaException, LogHandlerException {
		ArrayList<Pizza> pizzaLogTest = new ArrayList<Pizza>();
		pizzaLogTest = LogHandler.populatePizzaDataset("logs/20170101.txt");
		assertEquals(3, pizzaLogTest.size());
	}
	
	//Test that example dataset 2 can be created
	@Test
	public void createPizzaDataset2() throws PizzaException, LogHandlerException {
		ArrayList<Pizza> pizzaLogTest = new ArrayList<Pizza>();
		pizzaLogTest = LogHandler.populatePizzaDataset("logs/20170102.txt");
		assertEquals(10, pizzaLogTest.size());
	}
	
	//Test that example dataset 3 can be created
	@Test
	public void createPizzaDataset3() throws PizzaException, LogHandlerException {
		ArrayList<Pizza> pizzaLogTest = new ArrayList<Pizza>();
		pizzaLogTest = LogHandler.populatePizzaDataset("logs/20170103.txt");
		assertEquals(100, pizzaLogTest.size());
	}
	
	//Test the individual create pizza from line method
	@Test
	public void testCreatePizza() throws PizzaException, LogHandlerException {
		Pizza testPizza = LogHandler.createPizza("19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2");
		assertEquals(2, testPizza.getQuantity());
	}
	
	//Test that an exception is thrown when an invalid time format is entered
	@Test(expected = LogHandlerException.class) 
	public void invalidTimeFormat() throws PizzaException, LogHandlerException {
		Pizza testPizza = LogHandler.createPizza("19:002:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2");
	}
	
	//Test that an exception is thrown when an invalid time format is entered
	@Test(expected = LogHandlerException.class) 
	public void invalidTimeFormatDelivery() throws PizzaException, LogHandlerException {
		Pizza testPizza = LogHandler.createPizza("19:00:00,19:200:00,Casey Jones,0123456789,DVC,5,5,PZV,2");
	}
	
	//Test that an exception is rethrown
	@Test(expected = PizzaException.class) 
	public void invalidPizzaTypeCreatePizza() throws PizzaException, LogHandlerException {
		Pizza testPizza = LogHandler.createPizza("19:00:00,19:20:00,Casey Jones,0123456789,DVC,5,5,PIZZA PLEASE,2");
	}
	
	//Test that an exception is rethrown
	@Test(expected = PizzaException.class) 
	public void pizzaThrownOutCreatePizza() throws PizzaException, LogHandlerException {
		Pizza testPizza = LogHandler.createPizza("19:00:00,20:20:00,Casey Jones,0123456789,DVC,5,5,PZV,2");
	}
	
	//Test that an exception is thrown when a file is not found
	@Test(expected = LogHandlerException.class)
	public void pizzaFileNotFoundException() throws PizzaException, LogHandlerException {
		ArrayList<Pizza> pizzaLogTest = new ArrayList<Pizza>();
		pizzaLogTest = LogHandler.populatePizzaDataset("logs/somelog.txt");
	}
	
	//Test that an exception is thrown when an IOException occurs
	@Test(expected = LogHandlerException.class)
	public void pizzaIOException() throws PizzaException, LogHandlerException, IOException {
		ArrayList<Pizza> pizzaLogTest = new ArrayList<Pizza>();
		File testFile = new File("logs/20170101.txt");
		final RandomAccessFile raFile = new RandomAccessFile(testFile, "rw");
		raFile.getChannel().lock();
		//Lock the file stream (file still in use), triggers exception
		pizzaLogTest = LogHandler.populatePizzaDataset("logs/20170101.txt");
	}
	
	//Test that an exception is rethrown
	@Test(expected = PizzaException.class) 
	public void invalidPizzaType() throws PizzaException, LogHandlerException, IOException {
		ArrayList<Pizza> pizzaLogTest = new ArrayList<Pizza>();
		pizzaLogTest = LogHandler.populatePizzaDataset("logs/test.txt");
	}
	
	//Test that an exception is rethrown
	@Test(expected = PizzaException.class) 
	public void pizzaThrownOut() throws PizzaException, LogHandlerException, IOException {
		ArrayList<Pizza> pizzaLogTest = new ArrayList<Pizza>();
		pizzaLogTest = LogHandler.populatePizzaDataset("logs/test2.txt");
	}
	
	//Test that an exception is rethrown
	@Test(expected = PizzaException.class) 
	public void pizzaShopNotOpen() throws PizzaException, LogHandlerException, IOException {
		ArrayList<Pizza> pizzaLogTest = new ArrayList<Pizza>();
		pizzaLogTest = LogHandler.populatePizzaDataset("logs/test3.txt");
	}
	
}
