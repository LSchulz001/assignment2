package asgn2Tests;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Restaurant.LogHandler;
import asgn2Restaurant.PizzaRestaurant;

/**
 * A class that that tests the methods relating to the handling of Customer objects in the asgn2Restaurant.PizzaRestaurant
 * class as well as processLog and resetDetails.
 * 
 * @author n9699155
 */
public class RestaurantCustomerTests {
	PizzaRestaurant testRest;
	
	//Test that a  restaurant can be initialised, initialise it before every test
	@Test @Before
	public void CreateRestaurant() throws CustomerException, LogHandlerException {
		testRest = new PizzaRestaurant();
	}
	
	//Test that getNumCustomerOrders receives 0 for a restaurant with nothing in it
	@Test
	public void EmptyRestaurant() throws CustomerException, LogHandlerException {
		assertEquals(0, testRest.getNumCustomerOrders());
	}
	
	//Test getCustomerByIndex by retrieving two customers that are the same, then checking they are 
	@Test
	public void TestGetCustomer() throws CustomerException, LogHandlerException, PizzaException {
		testRest.processLog("logs/restTest.txt");
		assertTrue(testRest.getCustomerByIndex(0).equals(testRest.getCustomerByIndex(3)));
	}
	
	//Test getNumCustomerOrders with values
	@Test
	public void TestGetNumNotEmpty() throws CustomerException, LogHandlerException, PizzaException {
		testRest.processLog("logs/restTest.txt");
		assertEquals(6, testRest.getNumCustomerOrders());
	}
	
	//Test getTotalDeliveryDistance with several types of customers
	@Test
	public void TestGetTotalDistance() throws CustomerException, LogHandlerException, PizzaException {
		testRest.processLog("logs/restTest.txt");
		assertEquals(28.6, testRest.getTotalDeliveryDistance(), 0.1);
	}
	
	@Test //Test that the restaurant resets the values to nothing
	public void TestRestaurantReset() throws CustomerException, LogHandlerException, PizzaException {
		TestGetTotalDistance();
		testRest.resetDetails();
		EmptyRestaurant();
	}
}
