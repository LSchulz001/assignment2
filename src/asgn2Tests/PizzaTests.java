package asgn2Tests;

import org.junit.Test;

import asgn2Exceptions.PizzaException;

import static org.junit.Assert.*;

import java.time.LocalTime;
import java.time.ZoneId;

import asgn2Pizzas.MargheritaPizza;
import asgn2Pizzas.MeatLoversPizza;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;
import asgn2Pizzas.PizzaTopping;
import asgn2Pizzas.VegetarianPizza;

/**
 * A class that that tests the asgn2Pizzas.MargheritaPizza, asgn2Pizzas.VegetarianPizza, asgn2Pizzas.MeatLoversPizza classes. 
 * Note that an instance of asgn2Pizzas.MeatLoversPizza should be used to test the functionality of the 
 * asgn2Pizzas.Pizza abstract class. 
 * 
 * @author n9702911
 *
 */
public class PizzaTests {
	
	//Test if a basic meat lovers pizza can be created
	@Test
	public void createMeatPizza() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MeatLoversPizza testMeat = new MeatLoversPizza(1, now1, now2);
		assertEquals("Meat Lovers", testMeat.getPizzaType());
	}
	
	//Test if a basic margherita pizza can be created
	@Test
	public void createMargheritaPizza() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MargheritaPizza testMeat = new MargheritaPizza(1, now1, now2);
		assertEquals("Margherita", testMeat.getPizzaType());
	}
	
	//Test if a basic vegetarian pizza can be created
	@Test
	public void createVegetarianPizza() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		VegetarianPizza testMeat = new VegetarianPizza(1, now1, now2);
		assertEquals("Vegetarian", testMeat.getPizzaType());
	}
	
	//Test if meat lovers cost calculates correctly
	@Test
	public void meatPizzaCost() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MeatLoversPizza testMeat = new MeatLoversPizza(1, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(5.0, testMeat.getCostPerPizza(), 0.1);
	}
	
	//Test if margherita cost calculates correctly
	@Test
	public void margheritaPizzaCost() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MargheritaPizza testMeat = new MargheritaPizza(1, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(1.5, testMeat.getCostPerPizza(), 0.1);
	}
	
	//Test if multiple pizza's doesn't affect individual pizza cost
	@Test
	public void multiplePizzaCost() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MargheritaPizza testMeat = new MargheritaPizza(4, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(1.5, testMeat.getCostPerPizza(), 0.1);
	}
	
	//Test the an order cost of multiple pizza's calculates correctly
	@Test
	public void meatPizzaOrderCost() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MargheritaPizza testMeat = new MargheritaPizza(4, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(6, testMeat.getOrderCost(), 0.1);
	}
	
	//Test if vegetarian cost calculates correctly
	@Test
	public void vegetarianPizzaCost() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		VegetarianPizza testMeat = new VegetarianPizza(1, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(5.5, testMeat.getCostPerPizza(), 0.1);
	}
	
	//Test the price of a meat lovers pizza calculates correctly
	@Test
	public void meatPizzaPrice() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MeatLoversPizza testMeat = new MeatLoversPizza(1, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(12, testMeat.getPricePerPizza(), 0.1);
	}
	
	//Test the price of a margherita pizza calculates correctly
	@Test
	public void margheritaPizzaPrice() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MargheritaPizza testMeat = new MargheritaPizza(1, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(8, testMeat.getPricePerPizza(), 0.1);
	}
	
	//Test the price of a vegetarian pizza calculates correctly
	@Test
	public void vegePizzaPrice() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		VegetarianPizza testMeat = new VegetarianPizza(1, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(10, testMeat.getPricePerPizza(), 0.1);
	}
	
	//Test if multiple pizza's doesn't affect individual pizza price
	@Test
	public void multiplePizzaPrice() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MeatLoversPizza testMeat = new MeatLoversPizza(5, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(12, testMeat.getPricePerPizza(), 0.1);
	}
	
	//Test if multiple pizza's price calculates correctly
	@Test
	public void pizzaOrderPrice() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MeatLoversPizza testMeat = new MeatLoversPizza(5, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(60, testMeat.getOrderPrice(), 0.1);
	}
	
	//Test if a single pizza's profit calculates correctly
	@Test
	public void testOrderProfit() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MeatLoversPizza testMeat = new MeatLoversPizza(1, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(7, testMeat.getOrderProfit(), 0.1);
	}
	
	//Test multiple pizza's calculate profit correctly
	@Test
	public void testOrderProfitMultiple() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MeatLoversPizza testMeat = new MeatLoversPizza(3, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(21, testMeat.getOrderProfit(), 0.1);
	}
	
	//Test multiple vegetarian pizza's calculate profit correctly
	@Test
	public void testOrderProfitMultipleVege() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		VegetarianPizza testMeat = new VegetarianPizza(3, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(13.5, testMeat.getOrderProfit(), 0.1);
	}
	
	//Test multiple margherita pizza's calculate profit correctly
	@Test
	public void testOrderProfitMultipleMargherita() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MargheritaPizza testMeat = new MargheritaPizza(3, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(19.5, testMeat.getOrderProfit(), 0.1);
	}
	
	//Test multiple pizza's return correct quantity
	@Test
	public void multiplePizzaQuantity() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MeatLoversPizza testMeat = new MeatLoversPizza(5, now1, now2);
		testMeat.calculateCostPerPizza();
		assertEquals(5, testMeat.getQuantity());
	}	
	
	//Test if meat lovers contains the right toppings
	@Test
	public void containsToppingsMeat() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MeatLoversPizza testMeat = new MeatLoversPizza(5, now1, now2);
		testMeat.calculateCostPerPizza();
		assertTrue(testMeat.containsTopping(PizzaTopping.TOMATO));
		assertTrue(testMeat.containsTopping(PizzaTopping.CHEESE));
		assertTrue(testMeat.containsTopping(PizzaTopping.BACON));
		assertTrue(testMeat.containsTopping(PizzaTopping.PEPPERONI));
		assertTrue(testMeat.containsTopping(PizzaTopping.SALAMI));
	}
	
	//Test if margherita contains the right toppings
	@Test
	public void containsToppingsMargherita() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MargheritaPizza testMeat = new MargheritaPizza(5, now1, now2);
		testMeat.calculateCostPerPizza();
		assertTrue(testMeat.containsTopping(PizzaTopping.TOMATO));
		assertTrue(testMeat.containsTopping(PizzaTopping.CHEESE));
	}
	
	//Test if vegetarian contains the right toppings
	@Test
	public void containsToppingsVege() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		VegetarianPizza testMeat = new VegetarianPizza(5, now1, now2);
		testMeat.calculateCostPerPizza();
		assertTrue(testMeat.containsTopping(PizzaTopping.TOMATO));
		assertTrue(testMeat.containsTopping(PizzaTopping.CHEESE));
		assertTrue(testMeat.containsTopping(PizzaTopping.EGGPLANT));
		assertTrue(testMeat.containsTopping(PizzaTopping.MUSHROOM));
		assertTrue(testMeat.containsTopping(PizzaTopping.CAPSICUM));
	}
	
	//Test if meat lovers does not contain any wrong toppings 
	@Test
	public void doesNotContainToppingsMeat() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MeatLoversPizza testMeat = new MeatLoversPizza(5, now1, now2);
		testMeat.calculateCostPerPizza();
		assertFalse(testMeat.containsTopping(PizzaTopping.EGGPLANT));
		assertFalse(testMeat.containsTopping(PizzaTopping.MUSHROOM));
		assertFalse(testMeat.containsTopping(PizzaTopping.CAPSICUM));
	}
	
	//Test if margherita does not contain any wrong toppings 
	@Test
	public void doesNotContainToppingsMargherita() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MargheritaPizza testMeat = new MargheritaPizza(5, now1, now2);
		testMeat.calculateCostPerPizza();
		assertFalse(testMeat.containsTopping(PizzaTopping.EGGPLANT));
		assertFalse(testMeat.containsTopping(PizzaTopping.MUSHROOM));
		assertFalse(testMeat.containsTopping(PizzaTopping.CAPSICUM));
		assertFalse(testMeat.containsTopping(PizzaTopping.BACON));
		assertFalse(testMeat.containsTopping(PizzaTopping.PEPPERONI));
		assertFalse(testMeat.containsTopping(PizzaTopping.SALAMI));
	}
	
	//Test if vegetarian does not contain any wrong toppings 
	@Test
	public void doesNotContainToppingsVegetarian() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		VegetarianPizza testMeat = new VegetarianPizza(5, now1, now2);
		testMeat.calculateCostPerPizza();
		assertFalse(testMeat.containsTopping(PizzaTopping.BACON));
		assertFalse(testMeat.containsTopping(PizzaTopping.PEPPERONI));
		assertFalse(testMeat.containsTopping(PizzaTopping.SALAMI));
	}
	
	//Test that an exception is thrown when an invalid amount of pizza's is entered
	@Test(expected = PizzaException.class) 
	public void lessThanPizzaAmount() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MeatLoversPizza testMeat = new MeatLoversPizza(0, now1, now2);
		testMeat.calculateCostPerPizza();
	}
	
	//Test that an exception is thrown when an invalid amount of pizza's is entered
	@Test(expected = PizzaException.class) 
	public void greaterThanPizzaAmount() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(19, 15);
		MeatLoversPizza testMeat = new MeatLoversPizza(11, now1, now2);
	}
	
	//Test that an exception is thrown when a pizza is 'thrown out'
	@Test(expected = PizzaException.class) 
	public void throwOutPizza() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 5);
	    LocalTime now2 = LocalTime.of(20, 6);
		MeatLoversPizza testMeat = new MeatLoversPizza(1, now1, now2);
	}
	
	//Test that an exception is thrown when the restaurant is 'closed'
	@Test(expected = PizzaException.class) 
	public void restaurantClosedMorning() throws PizzaException {
		LocalTime now1 = LocalTime.of(15, 5);
	    LocalTime now2 = LocalTime.of(15, 6);
		MeatLoversPizza testMeat = new MeatLoversPizza(1, now1, now2);
	}
	
	//Test that an exception is thrown when the restaurant is 'closed'
	@Test(expected = PizzaException.class) 
	public void restaurantClosedNight() throws PizzaException {
		LocalTime now1 = LocalTime.of(23, 5);
	    LocalTime now2 = LocalTime.of(23, 6);
		MeatLoversPizza testMeat = new MeatLoversPizza(1, now1, now2);
	}
	
	//Test that an exception is thrown when delivery time is before order time
	@Test(expected = PizzaException.class) 
	public void deliveryBeforeOrdert() throws PizzaException {
		LocalTime now1 = LocalTime.of(19, 7);
	    LocalTime now2 = LocalTime.of(19, 6);
		MeatLoversPizza testMeat = new MeatLoversPizza(1, now1, now2);
	}
	
}
