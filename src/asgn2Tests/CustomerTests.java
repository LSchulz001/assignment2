package asgn2Tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import asgn2Customers.DriverDeliveryCustomer;
import asgn2Customers.DroneDeliveryCustomer;
import asgn2Customers.PickUpCustomer;
import asgn2Exceptions.CustomerException;

/**
 * A class that tests the that tests the asgn2Customers.PickUpCustomer, asgn2Customers.DriverDeliveryCustomer,
 * asgn2Customers.DroneDeliveryCustomer classes. Note that an instance of asgn2Customers.DriverDeliveryCustomer 
 * should be used to test the functionality of the  asgn2Customers.Customer abstract class. 
 * 
 * @author n9699155
 * 
 *
 */
public class CustomerTests {
	//Test that we can successfully create each of the 3 types of customers
	@Test
	public void createDriverCustomer() throws CustomerException {
		DriverDeliveryCustomer cusTest = new DriverDeliveryCustomer("TestName", "0412341234", 5, 5);
		assertEquals("Driver Delivery", cusTest.getCustomerType());
	}
	
	@Test
	public void createDroneCustomer() throws CustomerException {
		DroneDeliveryCustomer cusTest = new DroneDeliveryCustomer("TestName", "0412341234", 5, 5);
		assertEquals("Drone Delivery", cusTest.getCustomerType());
	}
	
	@Test
	public void createPickupCustomer() throws CustomerException {
		PickUpCustomer cusTest = new PickUpCustomer("TestName", "0412341234", 0, 0);
		assertEquals("Pick Up", cusTest.getCustomerType());
	}
	
	//Test the getName function
	@Test
	public void testGetName() throws CustomerException {
		PickUpCustomer cusTest = new PickUpCustomer("TestName", "0412341234", 0, 0);
		assertEquals("TestName", cusTest.getName());
	}
	
	//Test the getMobileNumber function
	@Test
	public void testGetNumber() throws CustomerException {
		PickUpCustomer cusTest = new PickUpCustomer("TestName", "0412341234", 0, 0);
		assertEquals("0412341234", cusTest.getMobileNumber());
	}
	
	//Test getLocation with both positive and negative values
	@Test
	public void TestGetLocation() throws CustomerException {
		DroneDeliveryCustomer cusTest = new DroneDeliveryCustomer("TestName", "0412341234", 5, -7);
		assertEquals(5, cusTest.getLocationX());
		assertEquals(-7, cusTest.getLocationY());
	}
	
	//Test GetDeliveryDistance for a DriverDeliveryCustomer
	@Test
	public void TestGetDriveDistance() throws CustomerException {
		DriverDeliveryCustomer cusTest = new DriverDeliveryCustomer("TestName", "0412341234",-8, 4);
		assertEquals(12, cusTest.getDeliveryDistance(), 0.1);
	}

	//Test GetDeliveryDistance for a DroneDeliveryCustomer	
	@Test
	public void TestGetDroneDistance() throws CustomerException {
		DroneDeliveryCustomer cusTest = new DroneDeliveryCustomer("TestName", "0412341234", 6, -10);
		assertEquals(11.6, cusTest.getDeliveryDistance(), 0.1);
	}
	
	//Test GetDeliveryDistance for a PickUpCustomer
	@Test
	public void TestGetPickupDistance() throws CustomerException {
		PickUpCustomer cusTest = new PickUpCustomer("TestName", "0412341234", 0, 0);
		assertEquals(0, cusTest.getDeliveryDistance(), 0.1);
	}
	
	//Test that an exception is thrown when a delivery customer is at position 0,0 (At the store)
	@Test(expected = CustomerException.class)
	public void DontDeliver() throws CustomerException {
		DriverDeliveryCustomer cusTest = new DriverDeliveryCustomer("TestName", "0412341234", 0, 0);
	}
	
	//Test that an exception is thrown when a pick up customer is not at position 0,0
	@Test(expected = CustomerException.class)
	public void DontPickUp() throws CustomerException {
		PickUpCustomer cusTest = new PickUpCustomer("TestName", "0412341234", 1, 0);
	}
	
	//Test that an exception is thrown when a location is too far to deliver to with a driver customer
	@Test(expected = CustomerException.class)
	public void DriveTooFar() throws CustomerException {
		DriverDeliveryCustomer cusTest = new DriverDeliveryCustomer("TestName", "0412341234", 0, -11);
	}
	
	//Test that an exception is thrown when a location is too far to deliver to with a drone customer	
	@Test(expected = CustomerException.class)
	public void DroneTooFar() throws CustomerException {
		DroneDeliveryCustomer cusTest = new DroneDeliveryCustomer("TestName", "0412341234", 11, 0);
	}
	
	//Test that an exception is thrown when the phone number is larger than 10 digits
	@Test(expected = CustomerException.class)
	public void BadNumberBig() throws CustomerException {
		PickUpCustomer cusTest = new PickUpCustomer("TestName", "04123412341", 0, 0);
	}
	
	//Test that an exception is thrown when the phone number is smaller than 10 digits
	@Test(expected = CustomerException.class)
	public void BadNumberSmall() throws CustomerException {
		PickUpCustomer cusTest = new PickUpCustomer("TestName", "041234123", 0, 0);
	}
	
	//Test that an exception is thrown when there are NaN characters in the phone number
	@Test(expected = CustomerException.class)
	public void BadNumbercharacters() throws CustomerException {
		PickUpCustomer cusTesthigh = new PickUpCustomer("TestName", "04123412a4", 0, 0);
		PickUpCustomer cusTestlow = new PickUpCustomer("TestName", "041/34@2.4", 0, 0);
	}
	
	//Test that an exception is thrown when the customerName is too big
	@Test(expected = CustomerException.class)
	public void TooBigName() throws CustomerException {
		PickUpCustomer cusTesthigh = new PickUpCustomer("qwertyuiopasdfghjklzx", "0412341234", 0, 0);
	}
	
	//Test that an exception is thrown when the customerName is too small
	@Test(expected = CustomerException.class)
	public void TooSmallName() throws CustomerException {
		PickUpCustomer cusTesthigh = new PickUpCustomer("", "0412341234", 0, 0);
	}
	
	//Test that no exceptions are thrown when given name values right on the acceptable boundary
	@Test
	public void BoundaryName() throws CustomerException {
		PickUpCustomer cusTesthigh = new PickUpCustomer("qwertyuiopasdfghjklz", "0412341234", 0, 0);
		PickUpCustomer cusTestlow = new PickUpCustomer("1", "0412341234", 0, 0);
	}	
}
