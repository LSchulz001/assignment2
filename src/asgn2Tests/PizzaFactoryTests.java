package asgn2Tests;

import static org.junit.Assert.assertEquals;

import java.time.LocalTime;

import org.junit.Test;

import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;

/** 
 * A class that tests the asgn2Pizzas.PizzaFactory class.
 * 
 * @author n9702911
 * 
 */
public class PizzaFactoryTests {
	
	//Test that a meat lovers pizza can be made from factory
	@Test
	public void createMeatPizzaFromFactory() throws PizzaException {
	    LocalTime now1 = LocalTime.of(19, 5, 30);
	    LocalTime now2 = LocalTime.of(19, 15, 30);
		Pizza pizza = PizzaFactory.getPizza("PZL", 1, now1, now2);
		assertEquals("Meat Lovers", pizza.getPizzaType());
	}
	
	//Test that a margherita pizza can be made from factory
	@Test
	public void createMargheritaPizzaFromFactory() throws PizzaException {
	    LocalTime now1 = LocalTime.of(19, 5, 30);
	    LocalTime now2 = LocalTime.of(19, 15, 30);
		Pizza pizza = PizzaFactory.getPizza("PZM", 1, now1, now2);
		assertEquals("Margherita", pizza.getPizzaType());
	}
	
	//Test that a vegetarian pizza can be made from factory
	@Test
	public void createVegePizzaFromFactory() throws PizzaException {
	    LocalTime now1 = LocalTime.of(19, 5, 30);
	    LocalTime now2 = LocalTime.of(19, 15, 30);
		Pizza pizza = PizzaFactory.getPizza("PZV", 1, now1, now2);
		assertEquals("Vegetarian", pizza.getPizzaType());
	}
	
	//Test that a multiple pizza's can be made from factory
	@Test
	public void createMultiplePizzaFromFactory() throws PizzaException {
	    LocalTime now1 = LocalTime.of(19, 5, 30);
	    LocalTime now2 = LocalTime.of(19, 15, 30);
		Pizza pizza = PizzaFactory.getPizza("PZL", 4, now1, now2);
		pizza.calculateCostPerPizza();
		assertEquals(4, pizza.getQuantity());
	}
	
	//Test that an exception is thrown when the wrong pizza code is entered
	@Test(expected = PizzaException.class)
	public void wrongPizzaCode() throws PizzaException {
	    LocalTime now1 = LocalTime.of(19, 5, 30);
	    LocalTime now2 = LocalTime.of(19, 15, 30);
		Pizza pizza = PizzaFactory.getPizza("PZE", 2, now1, now2);
	}
	
	//Test that an exception is rethrown
	@Test(expected = PizzaException.class)
	public void pizzaQuantityOver() throws PizzaException {
	    LocalTime now1 = LocalTime.of(19, 5, 30);
	    LocalTime now2 = LocalTime.of(19, 15, 30);
		Pizza pizza = PizzaFactory.getPizza("PZL", 11, now1, now2);
	}
	
	//Test that an exception is rethrown
	@Test(expected = PizzaException.class) 
	public void pizzaThrownOut() throws PizzaException {
		 LocalTime now1 = LocalTime.of(19, 5, 30);
		 LocalTime now2 = LocalTime.of(20, 15, 30);
		 Pizza pizza = PizzaFactory.getPizza("PZL", 1, now1, now2);
	}
}
