package asgn2Restaurant;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.regex.PatternSyntaxException;

import asgn2Customers.Customer;
import asgn2Customers.CustomerFactory;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Pizzas.PizzaFactory;

/**
 *
 * A class that contains methods that use the information in the log file to return Pizza 
 * and Customer object - either as an individual Pizza/Customer object or as an
 * ArrayList of Pizza/Customer objects.
 * 
 * @author n9699155 and n9702911
 *
 */
public class LogHandler {
	private static final int ORDER_TIME = 0;
	private static final int DELIVERY_TIME = 1;
	private static final int CUSTOMER_NAME = 2;
	private static final int CUSTOMER_MOBILE = 3;
	private static final int CUSTOMER_CODE = 4;
	private static final int CUSTOMER_X = 5;
	private static final int CUSTOMER_Y = 6;
	private static final int PIZZA_CODE = 7;
	private static final int PIZZA_QUANTITY = 8;
	
	
	private static ArrayList<Customer> customers;
	private static ArrayList<Pizza> pizzas;

	/**
	 * Returns an ArrayList of Customer objects from the information contained in the log file ordered as they appear in the log file.
	 * @param filename The file name of the log file
	 * @return an ArrayList of Customer objects from the information contained in the log file ordered as they appear in the log file. 
	 * @throws CustomerException If the log file contains semantic errors leading that violate the customer constraints listed in Section 5.3 of the Assignment Specification or contain an invalid customer code (passed by another class).
	 * @throws LogHandlerException If the file could not be found (FileNotFoundException)
	 * @throws LogHandlerException If the file input is interrupted (IOException)
	 * 
	 */
	public static ArrayList<Customer> populateCustomerDataset(String filename) throws CustomerException, LogHandlerException{
		customers = new ArrayList<Customer>();
		BufferedReader br;
		
		//Loop through all the lines in the file and populate the ArrayList
		try {
			br = new BufferedReader(new FileReader(filename));
			String line;
			while ((line = br.readLine()) != null) {
		        customers.add(createCustomer(line));
		    }
			br.close();
		} catch (FileNotFoundException e) {
			//Throw an exception if a FileNotFoundException occurs
			throw new LogHandlerException("Could not find the selected file");
		} catch (IOException e) {
			//Throw an exception if a IOException occurs 
			throw new LogHandlerException("Input interrupted");
		} catch (CustomerException e) {
			//Rethrow an customer exceptions
			throw e;
		}

		return customers;
	}		

	/**
	 * Returns an ArrayList of Pizza objects from the information contained in the log file ordered as they appear in the log file. .
	 * @param filename The file name of the log file
	 * @return an ArrayList of Pizza objects from the information contained in the log file ordered as they appear in the log file. .
	 * @throws PizzaException If the log file contains semantic errors leading that violate the pizza constraints listed in Section 5.3 of the Assignment Specification or contain an invalid pizza code (passed by another class).
	 * @throws LogHandlerException If there was a problem with the log file not related to the semantic errors above
	 * 
	 */
	public static ArrayList<Pizza> populatePizzaDataset(String filename) throws PizzaException, LogHandlerException{
		pizzas = new ArrayList<Pizza>();
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(filename));
			String line;
			while ((line = br.readLine()) != null) {
		        pizzas.add(createPizza(line));
		    }
			br.close();
		} catch (FileNotFoundException e) {
			throw new LogHandlerException("Could not find the selected file");
		} catch (IOException e) {
			throw new LogHandlerException("Input interrupted");
		} catch (PizzaException e) {
			throw e;
		}
		return pizzas;
	}	

	
	/**
	 * Creates a Customer object by parsing the  information contained in a single line of the log file. The format of 
	 * each line is outlined in Section 5.3 of the Assignment Specification.  
	 * @param line - A line from the log file
	 * @return- A Customer object containing the information from the line in the log file
	 * @throws CustomerException - If the log file contains semantic errors leading that violate the customer constraints listed in Section 5.3 of the Assignment Specification or contain an invalid customer code (passed by another class).
	 * @throws LogHandlerException - If the customer location could not be parsed from the line.
	 */
	public static Customer createCustomer(String line) throws CustomerException, LogHandlerException{
		int locationX = 0;
		int locationY = 0;
		Customer cust = null;
		
		//Split the line at comma's and create a customer from the information
		try {
			String[] lineInfo = line.split(",");
			String customerCode = lineInfo[CUSTOMER_CODE];
			String name = lineInfo[CUSTOMER_NAME];
			String mobileNumber = lineInfo[CUSTOMER_MOBILE];
			locationX = Integer.parseInt(lineInfo[CUSTOMER_X]);
			locationY = Integer.parseInt(lineInfo[CUSTOMER_Y]);
			cust = CustomerFactory.getCustomer(customerCode, name, mobileNumber, locationX, locationY);
		} catch (NumberFormatException e) {
			//Throw a new exception if the customer location cannot be parsed to an integer
			throw new LogHandlerException("Could not pass customer location to integer");
		} catch(CustomerException e) {
			//Rethrow any customer exceptions
			throw e;
		}
		
		return cust;
	}
	
	/**
	 * Creates a Pizza object by parsing the information contained in a single line of the log file. The format of 
	 * each line is outlined in Section 5.3 of the Assignment Specification.  
	 * @param line - A line from the log file
	 * @return- A Pizza object containing the information from the line in the log file
	 * @throws PizzaException If the log file contains semantic errors leading that violate the pizza constraints listed in Section 5.3 of the Assignment Specification or contain an invalid pizza code (passed by another class).
	 * @throws LogHandlerException - If there was a problem parsing the line from the log file.
	 */
	public static Pizza createPizza(String line) throws PizzaException, LogHandlerException{
		int pizzaQuantity;
		LocalTime ordTime;
		LocalTime delTime;
		Pizza pizza = null;
		
		String[] lineInfo = line.split(",");
		String pizzaCode = lineInfo[PIZZA_CODE];
		
		try {
			pizzaQuantity = Integer.parseInt(lineInfo[PIZZA_QUANTITY]);
			ordTime = LocalTime.parse(lineInfo[ORDER_TIME]);
			delTime = LocalTime.parse(lineInfo[DELIVERY_TIME]);
			pizza = PizzaFactory.getPizza(pizzaCode, pizzaQuantity, ordTime, delTime);
		} catch (NumberFormatException e) {
			throw new LogHandlerException("Could not parse customer location to integer");
		} catch(PizzaException e) {
			throw e;
		} catch(DateTimeParseException e) {
			throw new LogHandlerException("Could not parse deliveryTime or OrderTime to LocalTime");
		}
		return pizza;
		
	}

}
