package asgn2GUIs;

import java.awt.event.ActionEvent;


import java.awt.event.ActionListener;
import java.io.File;
import java.text.DecimalFormat;

import javax.swing.text.DefaultCaret;

import asgn2Customers.Customer;
import asgn2Exceptions.CustomerException;
import asgn2Exceptions.LogHandlerException;
import asgn2Exceptions.PizzaException;
import asgn2Pizzas.Pizza;
import asgn2Restaurant.PizzaRestaurant;

import java.awt.*;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


/**
 * This class is the graphical user interface for the rest of the system. 
 * Currently it is a �dummy� class which extends JFrame and implements Runnable and ActionLister. 
 * It should contain an instance of an asgn2Restaurant.PizzaRestaurant object which you can use to 
 * interact with the rest of the system. You may choose to implement this class as you like, including changing 
 * its class signature � as long as it  maintains its core responsibility of acting as a GUI for the rest of the system. 
 * You can also use this class and asgn2Wizards.PizzaWizard to test your system as a whole
 * 
 * 
 * @author n9699155 and n9702911
 *
 */
public class PizzaGUI extends javax.swing.JFrame implements Runnable, ActionListener {
	private static final long serialVersionUID = -6125961265088249922L;
	private static final int WIDTH = 500;
	private static final int HEIGHT = 650;
	private JFileChooser fcLogChooser;
	private JButton btnOpenFileChooser;
	private JPanel pnlBtn;
	private JPanel pnlTitle;
	private JPanel pnlContent;
	private JLabel lblTitle;
	private JLabel lblError;
	private JLabel lblLoadLog;
	private JLabel lblDisplay;
	private JLabel lblCalculate;
	private JLabel lblReset;
	private JLabel lblDistance;
	private JLabel lblProfit;
	private JButton btnDisplayContents;
	private JButton btnCalculateCosts;
	private JButton btnReset;
	private JTable tblCustomerContents;
	private JTable tblPizzaContents;
	private JTextField tfCustomerDelivery;
	private JTextField tfPizzaProfit;
	private JScrollPane jsCustomerTable;
	private JScrollPane jsPizzaTable;
	private File selectedFile;
	private PizzaRestaurant restaurant;
	private double profit;
	private boolean error = false;
	
	/**
	 * Creates a new Pizza GUI with the specified title 
	 * @param title - The title for the supertype JFrame
	 */
	public PizzaGUI(String title) {
		// TO DO
		super(title);
	}
	
	private void createGUI() {
		restaurant = new PizzaRestaurant();
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		
		btnOpenFileChooser = new JButton();
		btnDisplayContents = new JButton();
		pnlBtn = new JPanel();
		pnlTitle = new JPanel();
		pnlContent = new JPanel();
		lblTitle = new JLabel("Pizza Restaurant");
		lblError = new JLabel();
		lblLoadLog = new JLabel("Load Log:");
		lblDisplay = new JLabel("Display Log Contents:");
		lblCalculate = new JLabel("Calculate Profit and Distance:");
		lblDistance = new JLabel("Total Distance(Blocks):");
		lblProfit = new JLabel("Total Proft(Dollars):");
		lblReset = new JLabel("Reset:");
		fcLogChooser = new JFileChooser();
		btnCalculateCosts = new JButton();
		tfCustomerDelivery = new JTextField();
		tfPizzaProfit = new JTextField();
		btnReset = new JButton();
		jsCustomerTable = new JScrollPane();
		
		//Add panels to place Swing objects
		this.getContentPane().add(pnlBtn, BorderLayout.SOUTH);
		this.getContentPane().add(pnlTitle, BorderLayout.NORTH);
		this.getContentPane().add(pnlContent, BorderLayout.CENTER);
		
		//Set the GUI up initially
		pnlBtn.add(lblLoadLog);
		pnlBtn.add(btnOpenFileChooser);
		pnlTitle.add(lblTitle);
		
		//Add action listeners to all buttons
		btnOpenFileChooser.addActionListener(this);
		btnDisplayContents.addActionListener(this);
		btnCalculateCosts.addActionListener(this);
		btnReset.addActionListener(this);
		this.setVisible(true);
	}
	
	private int fileChooser() {
		//Initialise the file chooser and set the filter
		FileNameExtensionFilter filter = new FileNameExtensionFilter("*.txt", "txt");
		fcLogChooser.setFileFilter(filter);
		int status = fcLogChooser.showOpenDialog(getParent());
		
		//Process the log if file selected
		if (status == JFileChooser.APPROVE_OPTION) {
			selectedFile = fcLogChooser.getSelectedFile();
			processData(selectedFile);
		} 
		return status;
	}
	
	private void displayContents() {
		
		//Setup the table headers and the array to hold the info from the log
	    String[] columnNames = {"Customer Name", "Mobile Number", "Customer Type", 
	    		"X Location", "Y location", "Delivery Distance"};
	    Object[][] tableData = new Object[restaurant.getNumCustomerOrders()][6];
	    String customerType = "string";
	    
	    String[] pizzaColumnNames = {"Pizza Type", "Quantity", "Order Price", 
	    		"Order Cost", "Order Profit"};
	    Object[][] pizzaTableData = new Object[restaurant.getNumPizzaOrders()][5];
	    String pizzaType = "string";
	    
		try {
			//Iterate through each row in the array and add tot he JTable
			for (int row = 0; row < restaurant.getNumCustomerOrders(); row++) {
				customerType = restaurant.getCustomerByIndex(row).getCustomerType();
				tableData[row][0] = restaurant.getCustomerByIndex(row).getName();
				tableData[row][1] = restaurant.getCustomerByIndex(row).getMobileNumber();
				switch(customerType) {
					case "PUC": customerType = "Pickup";
					case "DNC": customerType = "Drone Delivery";
					case "DVC": customerType = "Driver Delivery";
				} 
				tableData[row][2] = customerType;
				tableData[row][3] = restaurant.getCustomerByIndex(row).getLocationX();
				tableData[row][4] = restaurant.getCustomerByIndex(row).getLocationY();
				tableData[row][5] = String.format("%.2f", restaurant.getCustomerByIndex(row).getDeliveryDistance());
				
				pizzaType = restaurant.getPizzaByIndex(row).getPizzaType();
				switch(pizzaType) {
				case "PZM": pizzaType = "Margherita";
				case "PZL": pizzaType = "Meat Lovers";
				case "PZV": pizzaType = "Vegetarian";
				}
				pizzaTableData[row][0] = pizzaType;
				pizzaTableData[row][1] = restaurant.getPizzaByIndex(row).getQuantity();
				pizzaTableData[row][2] = restaurant.getPizzaByIndex(row).getOrderPrice();
				pizzaTableData[row][3] = restaurant.getPizzaByIndex(row).getOrderCost();
				restaurant.getPizzaByIndex(row).calculateCostPerPizza();
				pizzaTableData[row][4] = restaurant.getPizzaByIndex(row).getOrderProfit();
			}
		} catch (CustomerException e) {
			displayError(e);
		} catch (PizzaException e) {
			displayError(e);
		}
		
		//Setup the table and add put it in a JScrollPane
		DefaultTableModel model = new DefaultTableModel(tableData, columnNames);
		tblCustomerContents = new JTable(model);
		tblCustomerContents.setPreferredScrollableViewportSize(new Dimension(450,200));
		tblCustomerContents.setFillsViewportHeight(true);
		jsCustomerTable = new JScrollPane(tblCustomerContents);
		jsCustomerTable.setVisible(true);
		
		DefaultTableModel pizzaModel = new DefaultTableModel(pizzaTableData, pizzaColumnNames);
		tblPizzaContents = new JTable(pizzaModel);
		tblPizzaContents.setPreferredScrollableViewportSize(new Dimension(450,200));
		tblPizzaContents.setFillsViewportHeight(true);
		jsPizzaTable = new JScrollPane(tblPizzaContents);
		jsPizzaTable.setVisible(true);
		
		//Add buttons for displaying data and calculating costs
		pnlBtn.add(lblDisplay, BorderLayout.NORTH);
		pnlBtn.add(btnDisplayContents, BorderLayout.NORTH);
		pnlBtn.add(lblCalculate, BorderLayout.NORTH);
		pnlBtn.add(btnCalculateCosts, BorderLayout.NORTH);
		pnlBtn.add(lblReset, BorderLayout.NORTH);
		pnlBtn.add(btnReset, BorderLayout.NORTH);
	}
	
	private void calculateCosts() {
		pnlBtn.remove(lblCalculate);
		pnlBtn.remove(btnCalculateCosts);
		String profitAsString;
		String deliveryDistance;
		
		//Get the total delivery distance and parse to string
		deliveryDistance = String.format("%.2f", restaurant.getTotalDeliveryDistance());
		//Get the total Profit and parse to string
		profit = restaurant.getTotalProfit();
		profitAsString = Double.toString(profit);
		
		//Add the information to the GUIs
		tfCustomerDelivery.setText(deliveryDistance);
		tfPizzaProfit.setText(profitAsString);
		pnlContent.add(lblDistance, BorderLayout.SOUTH);
		pnlContent.add(tfCustomerDelivery, BorderLayout.SOUTH);
		pnlContent.add(lblProfit, BorderLayout.SOUTH);
		pnlContent.add(tfPizzaProfit, BorderLayout.SOUTH);
		
	}
	
	private void reset() {
		//Reset the GUI to its initial state
		restaurant.resetDetails();
		pnlBtn.remove(lblReset);
		pnlBtn.remove(btnReset);
		pnlContent.remove(tfCustomerDelivery);
		pnlContent.remove(jsCustomerTable);
		pnlContent.remove(tfPizzaProfit);
		pnlContent.remove(jsPizzaTable);
		pnlContent.remove(lblDistance);
		pnlContent.remove(lblProfit);
		pnlBtn.remove(lblCalculate);
		pnlBtn.remove(btnCalculateCosts);
		pnlBtn.remove(lblDisplay);
		pnlBtn.remove(btnDisplayContents);
		pnlBtn.add(lblLoadLog);
		pnlBtn.add(btnOpenFileChooser);
		if(error) {
			error = false;
		}
	}
	
	private void displayError(Exception exception) {
		//Don't populate the dataset
		restaurant.resetDetails();
		//Display the error
		lblError.setText(exception.getMessage());
		pnlContent.add(lblError);
		error = true;
		//Reset the GUI
		//reset();
	}
 	
	private void processData(File file) {
		try {
			restaurant.processLog(file.getPath());
		} catch (CustomerException | PizzaException | LogHandlerException e) {
			displayError(e);
		}
	}
	
	private void repaintGUI(){
		this.revalidate();
		this.repaint();
	}
	
	@Override
	public void run() {
		// TO DO
		createGUI();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		int status = 0;
		Object src= arg0.getSource();
		if(src == btnOpenFileChooser) {
			status = fileChooser();
			repaintGUI();
			if(status == JFileChooser.APPROVE_OPTION && !error) {
				pnlContent.remove(lblError);
				pnlBtn.remove(lblLoadLog);
				pnlBtn.remove(btnOpenFileChooser);
				displayContents();
				repaintGUI();
			} else {
				pnlBtn.add(lblLoadLog);
				pnlBtn.add(btnOpenFileChooser);
				repaintGUI();
			}
		} else if(src == btnCalculateCosts) {
			repaintGUI();
			if(!error){
				calculateCosts();
			}
			repaintGUI();
		} else if(src == btnReset) {
			repaintGUI();
			reset();
			repaintGUI();
		} else if(src == btnDisplayContents) {
			pnlContent.add(jsCustomerTable, BorderLayout.NORTH);
			pnlContent.add(jsPizzaTable);
			pnlBtn.remove(lblDisplay);
			pnlBtn.remove(btnDisplayContents);
			repaintGUI();
		}
	}
}

